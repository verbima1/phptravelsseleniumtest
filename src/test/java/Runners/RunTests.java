package Runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(
        features = "src/test/resources/Features/scenarios.feature",
        glue = {"StepDefinitions"}
)
public class RunTests extends AbstractTestNGCucumberTests{

}

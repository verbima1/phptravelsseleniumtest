package Untils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browser {

    public static WebDriver driver;

    public static void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/Browsers/chromedriver.exe");
        driver = new ChromeDriver();
    }

    public static void closeBrowser() {
        driver.quit();
    }

}

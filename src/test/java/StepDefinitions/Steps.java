package StepDefinitions;

import Untils.Browser;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static Untils.Browser.driver;

public class Steps {
    public String newlyCreatedAccountEmail;

    @Before
    public static void setupDriver() {
        Browser.openBrowser();
    }
    @After
    public static void closeDriver(){
        Browser.closeBrowser();
    }

    @Given("the user is on PHP Travels homepage with accepted cookies")
    public void theUserIsOnPHPTravelsHomepage() throws InterruptedException {
        driver.manage().window().maximize();
        driver.get("https://www.phptravels.net/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String homePageTitle = driver.getTitle();
        Assert.assertEquals(homePageTitle, "PHPTRAVELS | Travel Technology Partner - PHPTRAVELS");
        WebElement acceptCookiesButton = driver.findElement(By.id("cookie_stop"));
        acceptCookiesButton.click();
        Thread.sleep(1000);
    }
    @Then("ensure that the account is successfully logged in")
    public void ensureThatTheAccountIsSuccessfullyLoggedIn() {
        String userDashboardTitle = driver.getTitle();
        Assert.assertEquals(userDashboardTitle, "Dashboard - PHPTRAVELS");
    }
    @Then("ensure that the login is unsuccessful")
    public void ensureThatTheLoginIsUnsuccessful() {
        String loginPageTitle = driver.getTitle();
        Assert.assertEquals(loginPageTitle, "Login - PHPTRAVELS");
    }
    @Then("ensure that the account is successfully logged out")
    public void ensureThatTheAccountIsSuccessfullyLoggedOut() {
        String loginPageTitle = driver.getTitle();
        Assert.assertEquals(loginPageTitle, "Login - PHPTRAVELS");
    }
    @When("he navigates to the sign in page")
    public void heNavigatesToTheSignInPage() {
        WebElement signInButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/header/div[1]/div/div/div[2]/div/div/a[2]"));
        signInButton.click();
        String loginPageTitle = driver.getTitle();
        Assert.assertEquals(loginPageTitle, "Login - PHPTRAVELS");
    }
    @And("he enters {string} as account email")
    public void heEntersAsAccountEmail(String email) {
        WebElement emailField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[1]/div/input"));
        emailField.sendKeys(email);
    }

    @And("he enters {string} as a password")
    public void heEntersAsAPassword(String password) {
        WebElement passwordField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[2]/div[1]/input"));
        passwordField.sendKeys(password);
    }

    @And("he clicks Login button")
    public void heClicksLoginButton() {
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[3]/button"));
        loginButton.click();
    }
    @And("he clicks on Logout button")
    public void heClicksOnLogoutButton() {
        WebElement logOutButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[3]/ul/li[5]/a"));
        logOutButton.click();
    }
    @When("he navigates to the sign up page")
    public void heNavigatesToTheSignUpPage() {
        WebElement signupButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/header/div[1]/div/div/div[2]/div/div/a[1]"));
        signupButton.click();
        String loginPageTitle = driver.getTitle();
        Assert.assertEquals(loginPageTitle, "Signup - PHPTRAVELS");
    }

    @And("he enters {string} as first name,{string} as last name and {string} as phone number")
    public void heEntersAsFirstNameAsLastNameAndAsPhoneNumber(String firstName, String lastName, String phoneNumber) {
        WebElement firstNameInputField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[1]/div/input"));
        WebElement lastNameInputField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[2]/div/input"));
        WebElement phoneNumberInputField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[3]/div/input"));
        firstNameInputField.sendKeys(firstName);
        lastNameInputField.sendKeys(lastName);
        phoneNumberInputField.sendKeys(phoneNumber);
    }

    @And("he enters email")
    public void heEntersEmail() {
        Random numGenerator = new Random();
        int randomNumber = numGenerator.nextInt(1000);
        String emailForSignup = "test" + randomNumber + "@gmail.com";
        newlyCreatedAccountEmail = emailForSignup;
        WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[4]/div/input"));
        emailInputField.sendKeys(emailForSignup);
    }

    @And("he enters {string} as password")
    public void heEntersAsPassword(String password) {
        WebElement passwordInputField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[5]/div/input"));
        passwordInputField.sendKeys(password);
    }

    @And("he clicks on Signup button")
    public void heClicksOnSignupButton() throws InterruptedException {
        Thread.sleep(3000);
        WebElement signupButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[7]/button"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", signupButton);
        //        signupButton.click();
    }

    @Then("ensure that the sign up is successful")
    public void ensureThatTheSignUpIsSuccessful() throws InterruptedException {
        Thread.sleep(3000);
        WebElement congratsMessage = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/div"));
        String congratsMessageText = congratsMessage.getText();
        Assert.assertEquals(congratsMessageText, "Signup successfull please login.");
    }
    @And("make sure that the sign into newly created account is successful")
    public void makeSureThatTheSignIntoNewlyCreatedAccountIsSuccessful() {
        WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[1]/div/input"));
        emailInputField.sendKeys(newlyCreatedAccountEmail);
        WebElement passwordInputField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[2]/div[1]/input"));
        passwordInputField.sendKeys("123456");
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[3]/button"));
        loginButton.click();
        String userDashboardTitle = driver.getTitle();
        Assert.assertEquals(userDashboardTitle, "Dashboard - PHPTRAVELS");
    }
    @And("he enters {string} as already used email")
    public void heEntersAlreadyUsedEmail(String usedEmail) {
        WebElement emailInputField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[4]/div/input"));
        emailInputField.sendKeys(usedEmail);
    }
    @Then("ensure that the sign up is unsuccessful due to already existing user")
    public void ensureThatTheSignUpIsUnsuccessfulDueToAlreadyExistingUser() throws InterruptedException {
        Thread.sleep(3000);
        WebElement errorMessage = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/div[1]"));
        String errorMessageText = errorMessage.getText();
        Assert.assertEquals(errorMessageText, "Email already exist!");
    }

}

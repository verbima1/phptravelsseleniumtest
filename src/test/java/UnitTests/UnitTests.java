package UnitTests;

import Untils.Browser;
import static Untils.Browser.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class UnitTests {
    @BeforeSuite(alwaysRun = true)
    public void setupBeforeSuite() {
        Browser.openBrowser();
    }

    @AfterSuite(alwaysRun = true)
    public void setupAfterSuite() {
        Browser.closeBrowser();
    }

    @Test(description = "Empty flight form send Unit Test")
    public void emptyFromSendTest(){
        String email = "user@phptravels.com";
        String password = "password";

        driver.manage().window().maximize();
        driver.get("https://www.phptravels.net/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement signInButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/header/div[1]/div/div/div[2]/div/div/a[2]"));
        signInButton.click();
        WebElement emailField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[1]/div/input"));
        emailField.sendKeys(email);
        WebElement passwordField = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[2]/div[1]/input"));
        passwordField.sendKeys(password);
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/div[1]/div/div[2]/div[2]/div/form/div[3]/button"));
        loginButton.click();
        WebElement flightButton = driver.findElement(By.xpath("//*[@id=\"fadein\"]/header/div[2]/div/div/div/div/div[2]/nav/ul/li[3]/a"));
        flightButton.click();

        driver.get("https://www.phptravels.net/flights/en/usd/cgk/dps/oneway/economy/28-05-2022/1/0/0");
        WebElement choose = driver.findElement(By.xpath("//*[@id=\"fadein\"]/main/div/div[2]/section/ul/li[1]/div/form/div/div[2]/div"));
        choose.click();
        WebElement pick = driver.findElement(By.xpath("//*[@id=\"agreechb\"]"));
        pick.click();
        WebElement book = driver.findElement(By.xpath("//*[@id=\"booking\"]"));
        book.click();
        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle, "Flight Booking - PHPTRAVELS");
    }

    @Test(description = "")
    public void test1(){

    }

    @Test(description = "")
    public void test2(){

    }

    @Test(description = "")
    public void test3(){

    }

    @Test(description = "")
    public void test4(){

    }

    @Test(description = "")
    public void test5(){

    }
}